# `git range-diff` demo

Git 2.19 includes a new command, `range-diff`, that can compare two commit 
ranges, approximately for the purpose of answering the question, "did I screw 
up my rebase?". The command previously existed as a standalone but is now part 
of Git core.

This repository includes a small demo of the `range-diff` command. It includes 
two scripts:

- `init` which creates a new repository with a few branches that need to be 
  rebased into a linear history. Some of these branches deliberately conflict 
  and require resolution -- in all cases resolution should be in line with the 
  rebased history (removals, sorts).
- `t` is a test script that first executes `init`, then performs the intended 
  series of `rebase`, `mergetool`, and `range-diff` invocations. Conflicts 
  still have to be solved manually.

Previously we could have used something like

```
$ git log --oneline --left-right --cherry-mark \
    reduce-buzzwords@{1}...reduce-buzzwords
= 5173d3c Downsize buzzwords
= 4fcd02d (reduce-buzzwords) Downsize buzzwords
> 1664059 (master) Record more buzzwords
```

to determine if a rebased commit series was logically unchanged, but if it 
_was_ changed we'd have to manually inspect the altered commits to determine 
the differences. Here's a comparable `range-diff` invocation:

```sh
$ git range-diff master reduce-buzzwords@{1} reduce-buzzwords
1:  5173d3c = 1:  4fcd02d Downsize buzzwords
```

`range-diff` even shows changes in the commit message! To see this, try running 
`git rebase --interactive sort all-buzzwords` and changing one of the messages.
