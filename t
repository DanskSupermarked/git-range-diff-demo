#!/bin/bash

set -o errexit
set -o nounset

. ./init

t()
{
    set +e
    set -x

    git rebase master reduce-buzzwords
    git range-diff master @{1} @

    git rebase reduce-buzzwords sort
    git mergetool
    git range-diff reduce-buzzwords @{1} @

    git rebase -i sort all-buzzwords
    git mergetool
    alphabetize buzzwords.txt
    git add -u
    git rebase --continue
    git mergetool
    alphabetize buzzwords.txt
    git add -u
    git rebase --continue
    git range-diff sort @{1} @
}

t
